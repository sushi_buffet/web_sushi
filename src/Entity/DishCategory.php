<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\This;
use Symfony\Component\DependencyInjection\Tests\Compiler\D;

/**
 * DishCategory
 *
 * @ORM\Table(name="dish_category")
 * @ORM\Entity(repositoryClass="App\Repository\DishCategoryRepository")
 */
class DishCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(name="description_title", type="string", length=255)
     */
    private $descriptionTitle;

    /**
     * @var string
     * @ORM\Column(name="description_content", type="string", length=255)
     */
    private $descriptionContent;

    /**
     * @ORM\OneToMany(targetEntity="Dish", mappedBy="dishCategory")
     */
    private $dishes;

    /**
     * @var boolean
     * @ORM\Column(name="popular", type="boolean")
     */
    private $popular;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dishes = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return DishCategory
     */
    public function setTitle(string $title): DishCategory
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return DishCategory
     */
    public function setId(int $id): DishCategory
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescriptionTitle(): string
    {
        return $this->descriptionTitle;
    }

    /**
     * @param string $descriptionTitle
     * @return DishCategory
     */
    public function setDescriptionTitle(string $descriptionTitle): DishCategory
    {
        $this->descriptionTitle = $descriptionTitle;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescriptionContent(): string
    {
        return $this->descriptionContent;
    }

    /**
     * @param string $descriptionContent
     * @return DishCategory
     */
    public function setDescriptionContent(string $descriptionContent): DishCategory
    {
        $this->descriptionContent = $descriptionContent;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPopular(): bool
    {
        return $this->popular;
    }

    /**
     * @param bool $popular
     * @return DishCategory
     */
    public function setPopular(bool $popular): DishCategory
    {
        $this->popular = $popular;

        return $this;
    }

}
