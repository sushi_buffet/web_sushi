<?php

namespace App\Services;

use App\Entity\DishCategory;
use App\Repository\DishCategoryRepository;

class DishCategoryService
{
    /** @var DishCategoryRepository */
    protected $dishCategoryRepository;

    /**
     * OrderService
     * onstructor.
     * @param DishCategoryRepository $dishCategoryRepository
     */
    public function __construct(DishCategoryRepository $dishCategoryRepository)
    {
        $this->dishCategoryRepository = $dishCategoryRepository;
    }

    /**
     * @param $id
     * @return DishCategory|null
     */
    public function get($id)
    {
        return $this->dishCategoryRepository->find($id);
    }

    /**
     * @return DishCategory[]
     */
    public function getAll()
    {
        return $this->dishCategoryRepository->findAll();
    }

    /**
     * @return DishCategory[]
     */
    public function getPopular()
    {
        return $this->dishCategoryRepository->findBy(['popular' => true]);
    }

    /**
     * @param DishCategory $dishCategory
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function save($dishCategory) :void
    {
        $this->dishCategoryRepository->persist($dishCategory)
            ->flush($dishCategory);
    }

    /**
     * @param DishCategory[] $dishCategories
     * @return array
     */
    public function serialize($dishCategories) : array
    {
        if (0 === count($dishCategories)) {
            return [];
        }

        $data = [];

        foreach ($dishCategories as $dishCategory) {
            $data[] = [
                'id' => $dishCategory->getId(),
                'title' => $dishCategory->getTitle(),
                'description_title' => $dishCategory->getDescriptionTitle(),
                'description_content' => $dishCategory->getDescriptionContent(),
                'is_popular' => $dishCategory->isPopular()
            ];
        }

        return $data;
    }

}