<?php

namespace App\Controller\Api;

use App\Services\DishCategoryService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categories")
 */
class CategoriesController extends Controller
{
    /** @var DishCategoryService */
    protected $dishCategoryService;

    /**
     * ApiController constructor.
     * @param DishCategoryService $dishCategoryService
     */
    public function __construct(
        DishCategoryService $dishCategoryService
    ) {
        $this->dishCategoryService = $dishCategoryService;
    }

    /**
     * @Route(path="/popular", methods={"GET"})
     * @return Response
     */
    public function popularAction()
    {
        $popularCategories = $this->dishCategoryService->getPopular();

        $data = $this->dishCategoryService->serialize($popularCategories);

        return new JsonResponse(
            $data
        );
    }

}