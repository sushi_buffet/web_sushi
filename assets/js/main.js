import React from 'react';
import ReactDOM from 'react-dom';
import Header from './components/header/index';
import CategoriesSection from './components/categories-section/index'

import './style.scss';

ReactDOM.render(
    <div className='wrapper'>
        <Header headerClassName = 'header-wrapper'/>
        <CategoriesSection/>
    </div>,
    document.getElementById('root')
);
