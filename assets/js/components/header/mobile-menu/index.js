import React, {Component} from 'react';

import ItemMenu from "../menu/item-menu";

import 'bootstrap/dist/css/bootstrap.min.css'
import './style.scss'


export default class MobileMenu extends Component {
    constructor(props) {
        super(props);

        this.state = {
            openMobileMenu: false
        };

        this.handleClickBurger = this.handleClickBurger.bind(this);
    }

    handleClickBurger() {
        this.setState({
            openMobileMenu: !this.state.openMobileMenu
        })
    }

    render() {
        const className = this.state.openMobileMenu ? 'mobile-menu-hide mobile-menu-show' : 'mobile-menu-hide';

        return (
            <div className = 'mobile-menu-wrapper'>
                <div onClick = {this.handleClickBurger} className = 'navbar-header'/>
                <div className = {className}>
                    <ul className='mobile-menu__nav-bar'>
                        <ItemMenu
                            className = 'nav-link mobile-menu__item-menu active'
                            urlAction = '#'
                            textItem = 'Главная'
                        />
                        <ItemMenu
                            className = 'nav-link mobile-menu__item-menu'
                            urlAction = '#'
                            textItem = 'Меню'
                        />
                        <ItemMenu
                            className = 'nav-link mobile-menu__item-menu'
                            urlAction = '#'
                            textItem = 'Галерея'
                        />
                        <ItemMenu
                            className = 'nav-link mobile-menu__item-menu'
                            urlAction = '#'
                            textItem = 'Корзина'
                        />
                        <ItemMenu
                            className = 'nav-link mobile-menu__item-menu'
                            urlAction = '#'
                            textItem = 'Контакты'
                        />
                    </ul>
                </div>
            </div>
        )

    }
}