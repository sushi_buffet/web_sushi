import React, {Component} from 'react';

import MapTop from './map-top/index'
import Menu from './menu/index'

import $ from 'jquery/dist/jquery.min'
import 'jquery/dist/jquery.min'

import './style.scss';

export default class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isScroll: false,
            width: $(document).innerWidth()
        };

        this.handleScroll = this.handleScroll.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll() {
        let isScroll =  $(document).scrollTop() > 7;

        this.setState({
            isScroll: isScroll
        });
    }


    render() {
        return (
            <header className = {
                !this.state.isScroll ? this.props.headerClassName: this.props.headerClassName + ' header-fixed'
            }
            >
                <MapTop/>
                <Menu
                    width = {this.state.width}
                />
            </header>
        )
    }
}