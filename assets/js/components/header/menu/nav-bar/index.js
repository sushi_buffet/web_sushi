import React, {Component} from 'react';

import ItemMenu from '../item-menu/index'

import 'bootstrap/dist/css/bootstrap.min.css'
import './style.scss'



export default class NavBar extends Component {

    render() {
        return (
            <div className="nav justify-content-end main-nav">
                        <ItemMenu
                            className = 'item-menu nav-link active'
                            urlAction = '/'
                            textItem = 'Главная'
                        />
                        <ItemMenu
                            className = 'item-menu nav-link'
                            urlAction = '#'
                            textItem = 'Меню'
                        />
                        <ItemMenu
                            className = 'item-menu nav-link'
                            urlAction = '#'
                            textItem = 'Галерея'
                        />
                        <ItemMenu
                            className = 'item-menu nav-link'
                            urlAction = '#'
                            textItem = 'Корзина'
                        />
                        <ItemMenu
                            className = 'item-menu nav-link'
                            urlAction = '#'
                            textItem = 'Контакты'
                        />
            </div>
        )
    }
}