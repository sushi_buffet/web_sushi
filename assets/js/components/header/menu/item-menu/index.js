import React, {Component} from 'react';

import 'bootstrap/dist/css/bootstrap.min.css'
import '../style.scss'


export default class ItemMenu extends Component {
    render() {
        return (
            <li className={this.props.className}>
                <a href={this.props.urlAction}>{this.props.textItem}</a>
            </li>
        )
    }
}