import React, {Component} from 'react';

import NavBar from './nav-bar/index'
import MobileMenu from '../mobile-menu/index'

import 'bootstrap/dist/css/bootstrap.min.css'
import './style.scss'


export default class Menu extends Component {

    render() {
       const menuBlock = this.props.width > 767 ? <NavBar/> : <MobileMenu/>;

        return (
            <div className='menu-wrapper container'>
                <div className='row justify-content-end'>
                    <div className="col-12">
                        {menuBlock}
                    </div>
                </div>
            </div>
        )
    }
}
