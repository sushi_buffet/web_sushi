import React from 'react';
import { Map, Marker } from 'yandex-map-react';

import './style.scss'

export default function ContactMap (props) {

        return (
            <div className='yandex-map'>
                <Map
                    onAPIAvailable={function () { }}
                    center={[55.754734, 37.583314]}
                    zoom={10} width={300}
                    height={150}
                >
                    <Marker lat={55.754734} lon={37.583314} />
                </Map>
            </div>
        );
}