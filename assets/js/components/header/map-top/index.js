import React, {Component} from 'react';

import MapPopup from './map-popup/index'

import './style.scss'

export default class MapTop extends Component {

    constructor(props) {
        super(props);

        this.state = {
            showPopupMap: false
        };

        this.showPopupMap = this.showPopupMap.bind(this);
    }

    showPopupMap() {
        this.setState({
            showPopupMap: !this.state.showPopupMap
        })
    }

    render() {
        return (
            <div className = 'geolocation'>
                <div onClick = {this.showPopupMap.bind(this)} className = "overlay"/>
                {this.state.showPopupMap &&
                    <MapPopup
                        closePopup = {this.showPopupMap.bind(this)}
                    />
                }
            </div>
        )
    }
}