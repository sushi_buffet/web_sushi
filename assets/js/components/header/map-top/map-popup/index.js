import React, {Component} from 'react'
import GoogleMap from '../google-map/index';

import './style.scss'

export default class MapPopup extends Component {
    render() {
        return (
            <div className = 'map-popup-wrapper'>
                <div className = 'map-popup'>
                    <span
                        className = 'map-popup__button'
                        onClick = {this.props.closePopup}
                    >X</span>
                    <div className = 'map-popup__information map-info'>
                        <h1 className='map-info__title'>Наш адрес</h1>
                        <span className='map-info__content'>Мате залки 44</span>
                        <h1 className='map-info__title'>Наш email</h1>
                        <span className='map-info__content'>
                            Email:
                            <span className='map-info__content_hover'>kolidenkova-katay@mail.ru</span>
                        </span>
                        <h1 className='map-info__title'>Наш телефон</h1>
                        <span className='map-info__content'>
                            Телефон заказа/доставки:
                            <span className='map-info__content_hover'>+7 (909) 874-72-77</span>
                        </span>
                    </div>
                    <div className='map-popup__map map'>
                        <h1 className='map-info__title'>Наше местоположение</h1>
                        <GoogleMap/>
                    </div>
                </div>
            </div>

        )
    }
}