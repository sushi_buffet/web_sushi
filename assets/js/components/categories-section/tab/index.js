import React, {Component} from 'react';

import $ from 'jquery/dist/jquery.min'
import 'jquery/dist/jquery.min'
import 'bootstrap/dist/css/bootstrap.min.css'

import './style.scss';


export default class Tab extends Component {

    render() {
        const itemsTab = this.props.popularCategories.map((category, index) =>
            <li
                key = {category.id}
                onClick={this.props.changeOpenTabId.bind(this, category.id)}
                className={
                    this.props.openTab === category.id ? 'active-tab' : 'my-tab'
                }
            >
                <span>{category.title}</span>
            </li>
        );

        return (
           <ul>
               {itemsTab}
           </ul>
        )
    }

}

