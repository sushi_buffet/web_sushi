import React, {Component} from 'react';

import $ from 'jquery/dist/jquery.min'
import 'jquery/dist/jquery.min'
import 'bootstrap/dist/css/bootstrap.min.css'

import './style.scss';


export default class DescriptionTab extends Component {

    render() {
        const showTab = this.props.popularCategories.map((category, index) => {
                if (category.id === this.props.openTab) {
                    return (
                        <div key={category.id} className="my-tab">
                            <div className="row">
                                <div className="col-12 col-md-10 offset-0 offset-md-1">
                                    <h2>{category.description_title}</h2>
                                    <p>{category.description_content}</p>
                                    <a className="btn btn-default btn-link" href="#" role="button">Подробнее</a>
                                </div>
                            </div>
                        </div>
                    )
                }
            }
        );

        return (
            <div className = "tab-content">
                {showTab}
            </div>
        )
    }

}