import React, {Component} from 'react';
import Tab from './tab/index'

import 'jquery/dist/jquery.min'
import 'bootstrap/dist/css/bootstrap.min.css'
import './style.scss';

import DescriptionTab from "./description-tab/index";


function Title(props) {
    return <h1>{props.titleProject}</h1>
}

function Description(props) {
    return <p className="slogan">{props.description}</p>
}

export default class CategoriesSection extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isOpenTab: true,
            openTab: 1,
            popularCategories: null,
            isLoaded: false
        };

        this.changeOpenTabId = this.changeOpenTabId.bind(this);
    }

    componentDidMount() {
        fetch("/api/categories/popular")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        popularCategories: result,
                        isLoaded: true
                    });
                }

            );
    }

    changeOpenTabId(openTab) {
        // console.log(openTab);
        this.setState({
            openTab: openTab
        })
    }

    render() {
        if (this.state.isLoaded) {
            return (
                <section className="services service-tab">
                    <div className="services-overlay">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-md-10 offset-0 offset-md-1 text-center">
                                    <Title titleProject = 'Добро пожаловать в Суши Буфет'/>
                                    <Description description = 'Мы предлагаем вам свежую и вкусную еду'/>
                                    <Tab
                                        changeOpenTabId = {this.changeOpenTabId}
                                        popularCategories = {this.state.popularCategories}
                                        openTab = {this.state.openTab}
                                    />
                                    <DescriptionTab
                                        popularCategories = {this.state.popularCategories}
                                        openTab = {this.state.openTab}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            )
        } else {
            return (
                <section className="services">
                    <div className="services-overlay">
                        Загрузка...
                    </div>
                </section>
            )
        }

    }

}
